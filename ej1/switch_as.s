.text
.arm
.global switch_as

switch_as:
	CMP R0, #1
	BNE CASO2 
	ADD R3,R1,R2
	MOV R0,R3
	B SALIR
CASO2:	CMP R0, #2
	BNE CASO3
	SUB R3,R1,R2
	MOV R0,R3
	B SALIR
CASO3:  CMP R0, #3
	BNE CASO4
	MUL R3,R1,R2
	MOV R0,R3
CASO4:	CMP R0, #4
	BNE SALIR
	AND R3,R1,R2
	MOV R0,R3
	B SALIR
SALIR:
	MOV PC,LR
	
