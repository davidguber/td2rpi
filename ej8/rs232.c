//gcc -lwiringPi -o rs232 rs232.c -lncurses
#include <termios.h>
#include <wiringPi.h> // -lwiringPi
#include <wiringSerial.h>
#include <ncurses.h> // -lncurses
#define PUERTO "/dev/serial0"
#define VELOCIDAD 9600

int main(void){
	int fd;
	struct termios configuracion;

	initscr();	// Inicializo mi pantalla de ncurses, por defecto llamada stdscr
	mvaddstr(0,0,"Envie un signo ! para finalizar"); // Le indico al usuario como cortar secuencia de luces
	move(1,0);
	wrefresh(stdscr);

	fd = serialOpen(PUERTO, VELOCIDAD);

	tcgetattr(fd, &configuracion);
	configuracion.c_cflag &= ~CSIZE;  // Enmascaro el tamaño
	configuracion.c_cflag &= ~PARENB; // Desatcivo la paridad
	configuracion.c_cflag &= ~CSTOPB; // Desactivo el segundo bit de stop
	configuracion.c_cflag |= CS8;	  // 8 bits
	tcsetattr(fd, TCSANOW, &configuracion);

	int car;

	while(car!='!'){
		if(serialDataAvail(fd)>0){
			car = serialGetchar(fd);
			serialPutchar(fd, car+1);
			addch(car);
			wrefresh(stdscr);
		}
	}

	serialClose(fd);
	endwin();	// Cierro la pantalla, necesario segun documentacion de ncurses
}
