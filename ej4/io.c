#include <stdio.h>
#include <wiringPi.h>
#include <ncurses.h>

bool out(){

int c,r;
	nodelay(stdscr,TRUE);
	noecho();

	c=getch();
		if(c==ERR){
		r = 0;
		}
		else{
		r = 1;
		}

	nodelay(stdscr,FALSE);
		
	return(r);
}


int main(void){

int i;
wiringPiSetup();

	pinMode(4 , OUTPUT);
	pinMode(5 , OUTPUT);
	pinMode(6 , OUTPUT);
	pinMode(26, OUTPUT);
	pinMode(27, OUTPUT);
	pinMode(28, OUTPUT);
	pinMode(29, OUTPUT);
	pinMode(25, OUTPUT);

	pinMode(0, INPUT);
	pinMode(2, INPUT);

	pinMode(21, INPUT);
	pinMode(22, INPUT);
	pinMode(23, INPUT);
	pinMode(24, INPUT);

	initscr();
	printw("Presione cualquier tecla para finalizar\n");

	bool array [8]={0,0,0,0,0,0,0,0};

	while(1){
		if(!out()){
			for (i=0;i<4;i++){
  			array[i] = digitalRead(21+i) & digitalRead(0);
			array[4+i] = digitalRead(21+i) & digitalRead(2);
			}
			digitalWrite(4 ,array[0]);
			digitalWrite(5 ,array[1]);
			digitalWrite(6 ,array[2]);
			digitalWrite(26,array[3]);
			digitalWrite(27,array[4]);
			digitalWrite(28,array[5]);
			digitalWrite(29,array[6]);
			digitalWrite(25,array[7]);
		}
		else{
			digitalWrite(4,LOW);
			digitalWrite(5,LOW);
			digitalWrite(6,LOW);
			digitalWrite(26,LOW);
			digitalWrite(27,LOW);
			digitalWrite(28,LOW);
			digitalWrite(29,LOW);
			digitalWrite(25,LOW);

			printw("Finalizo la ejecucion. Presione una tecla para continuar.");
			getch();
			break;
		}
	}
	endwin();
	return 0;
}
