#include <stdio.h>
#include <wiringPi.h>
#include <ncurses.h>

bool out(){

int c,r;
	nodelay(stdscr,TRUE);
	noecho();

	c=getch();
		if(c==ERR){
		r = 0;
		}
		else{
		r = 1;
		}

	nodelay(stdscr,FALSE);

	return(r);
}

int main()
{
wiringPiSetup();

int i;
bool array[12][8]={{0,0,0,0,0,0,0,1},
		  {0,0,0,0,0,0,1,0},
		  {0,0,0,0,0,1,0,0},
		  {0,0,0,0,1,0,0,0},
		  {0,0,0,0,1,0,0,1},
		  {0,0,0,1,0,0,1,0},
		  {0,0,0,1,0,1,0,0},
		  {0,0,1,0,1,0,0,0},
	       	  {0,0,1,1,0,0,0,0},
		  {0,1,1,0,0,0,0,0},
		  {1,1,0,0,0,0,0,0},
		  {1,0,0,0,0,0,0,0}};

	pinMode(4 , OUTPUT);
	pinMode(5 , OUTPUT);
	pinMode(6 , OUTPUT);
	pinMode(26, OUTPUT);
	pinMode(27, OUTPUT);
	pinMode(28, OUTPUT);
	pinMode(29, OUTPUT);
	pinMode(25, OUTPUT);

initscr();
printw("Presiona cualquier tecla para finalizar\n");

while(1){
	if (!out()){
		for (i=0;i<12;i++){
	
		digitalWrite(4 ,array[i][0]);
		digitalWrite(5 ,array[i][1]);
		digitalWrite(6 ,array[i][2]);
		digitalWrite(26,array[i][3]);
		digitalWrite(27,array[i][4]);
		digitalWrite(28,array[i][5]);
		digitalWrite(29,array[i][6]);
		digitalWrite(25,array[i][7]);
		delay (100);
		}
}

else{
			digitalWrite(4,LOW);
			digitalWrite(5,LOW);
			digitalWrite(6,LOW);
			digitalWrite(26,LOW);
			digitalWrite(27,LOW);
			digitalWrite(28,LOW);
			digitalWrite(29,LOW);
			digitalWrite(25,LOW);

			printw("Finalizo la ejecucion. Presione una tecla para continuar.");
			getch();
			break;
		}
}
endwin();
return 0;
}
