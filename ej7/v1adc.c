#include <wiringPi.h> // -lwiringPi
#include <pcf8591.h> // -lbcm2835
#include <stdio.h>

#define Address 0x48 // i2cdetect -y 1 nos da este valor
#define BASE 64
#define A0 BASE+0
#define A1 BASE+1
#define A2 BASE+2
#define A3 BASE+3

int main(void)
{
    int value;
    wiringPiSetup();
    pcf8591Setup(BASE,Address);

    while(1)
    {
        value = analogRead(A0);
	printf("A0 %d\n", value);
        value = analogRead(A1);
	printf("A1 %d\n", value);
        value = analogRead(A2);
	printf("A2 %d\n", value);
        value = analogRead(A3);
	printf("A3 %d\n", value);
        delay(500);
    }
}
