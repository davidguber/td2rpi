#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <termios.h>

#include <wiringPi.h>
#include <wiringSerial.h>


int main()
{
	int fd;
	int u_data = 0;
	char ch[100];
	struct termios configuracion;
	
	if ((fd = serialOpen ("/dev/ttyS0", 115200)) < 0)
 	{
 	   	fprintf (stderr, "Unable to open serial device: %s\n", strerror (errno));
 	   	return 1;
 	}

	if (wiringPiSetup () == -1)
	{
		fprintf (stdout, "Unable to start wiringPi: %s\n", strerror (errno));
	  	return 1;
	}
	
	tcgetattr(fd, &configuracion);
	configuracion.c_cflag &= ~CSIZE;  // Enmascaro el tamaño
	configuracion.c_cflag &= ~PARENB; // Desatcivo la paridad
	configuracion.c_cflag &= ~CSTOPB; // Desactivo el segundo bit de stop
	configuracion.c_cflag |= CS8;	  // 8 bits
	tcsetattr(fd, TCSANOW, &configuracion);
	
	delay(1000);
	printf("Iniciando recepcion UART:\n");
	
	while (u_data != -1)
	{
		if (serialDataAvail (fd) != -1)
		{
			u_data = serialGetchar(fd);
			printf("%3d\t", u_data);
			printf("%c\n", u_data);
		}
	}
	
	
	printf("Funcione\n");
	
	serialClose (fd);
	return 0;
}
