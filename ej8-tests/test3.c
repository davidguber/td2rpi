#include <stdio.h> 
#include <string.h> 
#include <errno.h>
#include <termios.h>

#include <wiringPi.h> 
#include <wiringSerial.h>

int main() {
	int fd;
	int u_data = 0;
	struct termios configuracion;
	
	if ((fd = serialOpen ("/dev/ttyS0", 115200)) < 0)
 	{
 	   	fprintf (stderr, "Unable to open serial device: %s\n", strerror (errno));
 	   	return 1;
 	}
	if (wiringPiSetup () == -1)
	{
		fprintf (stdout, "Unable to start wiringPi: %s\n", strerror (errno));
	  	return 1;
	}
	
	tcgetattr(fd, &configuracion);
	configuracion.c_cflag &= ~CSIZE;  // Enmascaro el tamaño
	configuracion.c_cflag &= ~PARENB; // Desatcivo la paridad
	configuracion.c_cflag &= ~CSTOPB; // Desactivo el segundo bit de stop
	configuracion.c_cflag |= CS8;	  // 8 bits
	tcsetattr(fd, TCSANOW, &configuracion);

	
	delay(1000);
	printf("Iniciando transmision UART:\n");
	
	serialPuts (fd, "Hola");
	
	
	printf("Funcione\n");
	
	serialClose (fd);
	return 0;
}
