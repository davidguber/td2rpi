#include <wiringPi.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "soft_lcd.h"

#define ROWS 4
#define COLS 4

#define LOCK 7
#define LOCK_ACTIVE HIGH
#define LOCK_INACTIVE LOW

#define MAX_PASS_LEN 8

char User[4];
char user1[] = {'1','2','3','4','\0'};
char user2[] = {'4','3','2','1','\0'};

char Pass[4];
char pass1[]={'5','6','7','8','5','6','7','8','\0'};
char pass2[]={'8','7','6','5','8','7','6','5','\0'};

int k = 0;
int l = 0;

char pressedKey = '\0';
int rowPins[ROWS] = {22, 23, 24, 25};
int colPins[COLS] = {21, 27, 28, 29};

char keys[ROWS][COLS] = {
   {'1', '2', '3', 'A'},
   {'4', '5', '6', 'B'},
   {'7', '8', '9', 'C'},
   {'*', '0', '#', 'D'}
};

void init_keypad()
{
   for (int c = 0; c < COLS; c++)
   {
      pinMode(colPins[c], OUTPUT);   
      digitalWrite(colPins[c], HIGH);
   }

   for (int r = 0; r < ROWS; r++)
   {
      pinMode(rowPins[r], INPUT);   
      pullUpDnControl(rowPins[r], PUD_UP);
   }
}

int findLowRow()
{
   for (int r = 0; r < ROWS; r++)
   {
      if (digitalRead(rowPins[r]) == LOW)
         return r;
   }

   return -1;
}

char get_key()
{
   int rowIndex;

   for (int c = 0; c < COLS; c++)
   {
      digitalWrite(colPins[c], LOW);

      rowIndex = findLowRow();
      if (rowIndex > -1)
      {
         if (!pressedKey)
            pressedKey = keys[rowIndex][c];
         return pressedKey;
      }

      digitalWrite(colPins[c], HIGH);
   }

   pressedKey = '\0';

   return pressedKey;
}

int main(void) 
{
   wiringPiSetup();

   init_keypad();
   
   lcd_t *lcd = lcd_create(9, 8, 0x27, 2);
	if (lcd == NULL) {
		printf("Cannot set-up LCD.\n");
	}
	pinMode(LOCK, OUTPUT);
	
	lcd_print(lcd, "Electronic Door");
    lcd_pos(lcd, 1, 0);
    lcd_print(lcd, "Lock Using RPI ");
	printf("User: %s / Pass = %s / 0\n", User, Pass);
	delay(5000);
	lcd_pos(lcd, 0, 0);
   while(1)
   {
      k = 0;
	  l = 0;
	  printf("User: %s / Pass = %s / 1\n", User, Pass);
	  User[0] = '0';
	  User[1] = '0';
	  User[2] = '0';
	  User[3] = '0';
	  //User[4] = '\0';
	  
	  Pass[0] = '0';
	  Pass[1] = '0';
	  Pass[2] = '0';
	  Pass[3] = '0';
	  //Pass[4] = '\0';
	  
	  printf("User: %s / Pass = %s / 2\n", User, Pass);
	  
	  lcd_clear(lcd);
	  lcd_print(lcd, "Usuario");
	  lcd_pos(lcd, 1, 0);
	  
	  while(k<4)
		{
			char x = get_key();
		
			if (x)
			{
				printf("pressed: %c\n", x);
				User[k] = x;
				printf("User: %s / Pass = %s\n", User, Pass);
				lcd_pos(lcd, 1, k);
				lcd_printf(lcd, "%c", x);
				k++;
			}
			else
				printf("no key pressed\n");
			delay(250);
		}
		printf("Llegue aca 1\n");
		printf("User = %s / user1 = %s\n", User, user1);
		if (strcmp(User,user1) == 0)
		{
		  lcd_clear(lcd);
		  lcd_print(lcd, "Contraseña");
		  lcd_pos(lcd, 1, 0);
		  printf("Llegue aca 3\n");
		  printf("Pass = %s / pass1 = %s\n", Pass, pass1);
		  while(l<MAX_PASS_LEN)
			{
				char x = get_key();
			
				if (x)
				{
					printf("pressed: %c\n", x);
					Pass[l] = x;
					lcd_pos(lcd, 1, l);
					lcd_printf(lcd, "*");
					l++;
				}
				else
					printf("no key pressed\n");
				delay(250);
			}
			printf("Llegue aca 4\n");
			printf("Pass = %s / pass1 = %s\n", Pass, pass1);
		  if (strcmp(Pass,pass1) == 0)
			{
				digitalWrite(LOCK, LOCK_ACTIVE);
				lcd_clear(lcd);
				lcd_print(lcd, "Puerta abierta");
				delay(5000);
				digitalWrite(LOCK, LOCK_INACTIVE);
			}
		  else
			{
				lcd_clear(lcd);
				lcd_print(lcd, "Error");
				printf("Llegue aca 5\n");
				delay(2000);
			}
		}
		else if (strcmp(User,user2) == 0)
		{
		  lcd_clear(lcd);
		  lcd_print(lcd, "Contraseña");
		  lcd_pos(lcd, 1, 0);
		  while(l<MAX_PASS_LEN)
			{
				char x = get_key();
			
				if (x)
				{
					printf("pressed: %c\n", x);
					Pass[l] = x;
					lcd_pos(lcd, 1, l);
					lcd_printf(lcd, "*");
					l++;
				}
				else
					printf("no key pressed\n");
				delay(250);
			}
		  if (strcmp(Pass,pass2) == 0)
			{
				digitalWrite(LOCK, LOCK_ACTIVE);
				lcd_clear(lcd);
				lcd_print(lcd, "Puerta abierta");
				delay(5000);
				digitalWrite(LOCK, LOCK_INACTIVE);
			}
		  else
			{
				lcd_clear(lcd);
				lcd_print(lcd, "Error");
				delay(2000);
			}
		}
		else
		{
			lcd_clear(lcd);
			lcd_print(lcd, "Error");
			printf("Llegue aca 10\n");
			delay(2000);
		}
   }
   
   lcd_destroy(lcd);

   return 0;
}
